import sys

sys.path.append("/home/site/wwwroot/playground")
import azure.functions as func
from .run import server

# from .app import application


def main(req: func.HttpRequest, context: func.Context) -> func.HttpResponse:
    return func.WsgiMiddleware(server).handle(req, context)
