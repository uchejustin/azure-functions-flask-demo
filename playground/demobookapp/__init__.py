# import flask - from the package import class
from flask import Flask, render_template

from flask_bootstrap import Bootstrap

app = Flask(__name__)
bootstrap = Bootstrap(app)

# create a function that creates a web application
# note that a web server will run this web application
def create_app():
    app.debug = True
    app.secret_key = "051ef4a7c70f5f5ca0817a81e202cbe09ce06febab0f5004936be43bbebb26c3"

    # initialize db with flask app

    # importing modules here to avoid circular references, register blueprints of routes
    from . import views

    app.register_blueprint(views.urlmanager)

    # app.register_blueprint(admin.bp)
    return app


@app.errorhandler(404)
# inbuilt function which takes error as parameter
def not_found(e):
    return render_template("404.html")


@app.errorhandler(500)
def internal_error(e):
    return render_template("500.html")
