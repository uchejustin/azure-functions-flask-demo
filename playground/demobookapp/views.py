from flask import Blueprint, render_template
import sys, time

urlmanager = Blueprint("urlmanagers", __name__)


@urlmanager.route("/api/playground/")
def index():
    return render_template(
        "index.html",
        contents=f"{time.ctime()},{sys.path}",
    )


@urlmanager.route("/api/playground/about/")
def about():
    return render_template("aboutus.html")
