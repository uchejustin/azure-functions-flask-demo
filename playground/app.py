import sys, os
from flask import Flask

application = Flask(__name__)


@application.route("/api/playground")
def home():
    return f"Hello, from Flask! {sys.path} and cwd path is {os.getcwd()} and files found are {os.listdir()}"


@application.route("/api/playground/test")
def hello():
    return "Hello world!"
